elog() {
  echo -e "\e[1m\e[36m :::" "$@" "\e[0m\e[39m"
}
eerr() {
  echo -e "\e[1m\e[33m !!!" "$@" "\e[0m\e[39m"
}
check_err() {
  RC=$?
  if [ $RC != 0 ]; then
    eerr "$@"
    exit $RC
  fi
}

fail() {
  echo "$*"
  exit 1
}

install_deb_with_deps() {
  pkgname=$(echo "$1" | awk -F_ '{print $1}')
  dpkg --force-depends --force-conflicts -i "$1" || true
  aptitude \
        -y \
        --without-recommends -o APT::Install-Recommends=false \
        -o Aptitude::ProblemResolver::StepScore=100 \
        -o "Aptitude::ProblemResolver::Hints::KeepDummy=reject ${pkgname} :UNINST" \
        -o Aptitude::ProblemResolver::Keep-All-Level=55000 \
        -o Aptitude::ProblemResolver::Remove-Essential-Level=maximum \
        install \
        "${pkgname}"
}

configure_apt() {
  REPOURL=${REPOURL:="repo.example.org/debian"}
  elog 'Configuring APT'
  if [ -n "${REPOS:-}" ] && [ "${branch}" != "master" ]; then
    cat >/etc/apt/sources.list.d/branch.list <<EOT
deb http://${REPOURL} ${REPOS} main
deb-src http://${REPOURL} ${REPOS} main
EOT
  fi
  # shellcheck disable=SC2046
  sed -e 's/^/    /' /etc/apt/sources.list $(find /etc/apt/sources.list.d -type f)
}

apt_update() {
  elog 'Updating packages'
  time apt-get update -q | true
  time apt-get upgrade -y "${APT_OPTIONS[@]}"
  check_err "Update using apt-get failed"
}

install_artifact_debs() {
  echo 'APT::Get::AllowUnauthenticated "true";' > /etc/apt/apt.conf.d/unauth.conf
  elog "Making local repo"
  packages=$(find . -iname '*.deb' -printf '%f\n' | awk -F_ '{print $1"="$2}')
  rm -rf .apt && mkdir .apt
  (
    cd .apt || exit
    apt-ftparchive packages .. > Packages
    apt-ftparchive release .. > Release
  )
  cat >/etc/apt/sources.list.d/local.list <<EOT
deb file:///${CI_PROJECT_DIR}/.apt/ ./
EOT

  apt_update

  elog "Installing built packages ${packages}"
  time apt-get install -y "${APT_OPTIONS[@]}" "${packages}"
  check_err "installing package failed, see above"
}

install_redis() {
  # Assume already installed by Dockerfile.
  elog 'Starting redis-server'
  service redis-server start
}

apt_install() {
  elog "Installing $*"
  time apt-get install -y "${APT_OPTIONS[@]}" "$@"
}
