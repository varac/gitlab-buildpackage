# Gitlab buildpackage docker image build scripts

We use our own base image (https://0xacab.org/leap/docker)
instead of the `debian:backports` one from dockerhub, because it has already
both LEAP (experimental) archive signing keys available.

Images are build and registered at https://0xacab.org/leap/gitlab-buildpackage/container_registry
on every push.

Below you can find the old, obsolete instructions how to build them
locally and push them manually.


# Manual build process (deprecated)

To build and upload to gitlab:

    docker login --username=varac 0xacab.org:4567
    ./publish

Package upgrades
----------------

Sometimes you need to upgrade packages in the base image,
so this is not done on every build, i.e. when security updates
are available.
Disable caching and build new images with:

    export DOCKER_BUILD_OPTS="--no-cache"
    ./publish

Assumptions
-----------

  * gitlab-multi-ci-runner with docker runner
  * gitlab docker registry enabled
  * gitlab 8.13+
  * only build for amd64


Notes
-----

- Real hostnames have been replaced with \*.example.org.
