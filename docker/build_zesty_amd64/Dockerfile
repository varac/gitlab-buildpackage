FROM 0xacab.org:4567/leap/docker/ubuntu:zesty_amd64

# Add nodejs LTS repo
RUN echo 'deb https://deb.nodesource.com/node_8.x zesty main' > /etc/apt/sources.list.d/nodesource.list
RUN curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -

RUN apt-get update && \
    apt-get -y dist-upgrade -y && \
    apt-get install -y --no-install-recommends \
    equivs \
    openssh-client \
    pep8 \
    build-essential \
    devscripts \
    dpkg-dev \
    fakeroot \
    git-buildpackage \
    pristine-tar \
    runit \
    aptitude \
    ca-certificates \
    apt-utils \
    libdistro-info-perl \
    dh-systemd \
    python-all \
    python-all-dev \
    python-wheel \
    python-setuptools \
    lintian \
    nodejs \
  && rm -rf /var/lib/apt/lists/*

RUN adduser --group --system build
RUN git config --global user.email 'sysdev@leap.se' && git config --global user.name "Automatic build"

COPY common/gbp-envdir /usr/local/gbp-envdir/
COPY common/etc-extras /etc/
COPY common/build-scripts /usr/local/sbin/

# Upgrade to latest npm to fix transient DNS lookup errors
RUN npm install npm@latest
